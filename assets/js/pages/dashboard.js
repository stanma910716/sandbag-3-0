$(document).ready(function(){
    Codebase.blocks("#event_block","state_loading")
    Codebase.blocks("#module_block","state_loading")
    Codebase.blocks("#scoreboard-block","state_loading")
    Codebase.blocks("#event-history-block","state_loading")
    Codebase.blocks("#now-score-block","state_loading")
})
$("#update_endpoint_btn").click(function (){
    let post_data = {
        "action":"update_endpoint",
        "token":sessionToken,
        "endpoint":$("#endpoint_field").val()
    }
    ws.send(JSON.stringify(post_data));
})