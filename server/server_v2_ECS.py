# metadata : http://169.254.169.254/latest/meta-data/
# region : http://169.254.169.254/latest/dynamic/instance-identity/document

from flask import Flask
from flask import Response, request
import json
import os
import requests
import time
from datetime import datetime
import hashlib
from threading import Thread
import configparser
from websocket import create_connection
app = Flask(__name__)

name = ""
count = 0


@app.route('/', methods=['GET', 'POST'])
def home():
    global count
    global file_location

    def after_response(name, request_id, countt):
        # do something that takes a long time
        global token
        global count
        ppname = name
        encrypt = hashlib.md5()
        encrypt.update(name.encode("utf-8"))
        name = encrypt.hexdigest()
        data = 3*countt
        time.sleep(3 * countt)
        count = count - 1
        ws = create_connection("wss://il2xe5wi9h.execute-api.us-east-1.amazonaws.com/dev")
        ws.send(json.dumps({
            "action": "send_score", "token": token, "answer": name, "RequestId": request_id
        }))
        print(json.dumps({
            "action": "send_score", "token": token, "answer": name, "RequestId": request_id, "wait": data,
            "getToken" : ppname
        }))
        # Send score <token, answer, version, request_id(from header)>
        ws.close()

    try:
        version = request.headers["Version"]
        name = request.headers["Token"]
        request_id = request.headers['RequestId']

        if os.path.isfile(file_location):
            file_exist = True
        else:
            open(file_location, "x")
        data = open(file_location, "r").read()
        with open(file_location, "w+") as log:
            nowtime = datetime.now().strftime('%m-%d-%Y %H:%M:%S')
            if version == "Refund":
                write_data = nowtime + " Warning: Unicorn Refund Request: " + name + "\n"
            if version == "WAF":
                write_data = nowtime + " Warning: Unicorn WAF Request: " + name + "\n"
            if version == "v1" or version == "v2" or version == "v3":
                write_data = nowtime + " Warning: Unicorn General Request: " + name + "\n"
            log.write(write_data + data)
        count += 1

        response = Response()
        # to lambda
        # response.headers["Token"] = name
        # response.headers["Version"] = "v1"
        # response.headers["From"] = "ECS"
        # response.headers["restime"] = str(datetime.now())
        # process = Process(target=after_response, args=(name, request_id, count))
        # process.start()
        thread = Thread(target=after_response, args=(name, request_id, count))
        thread.start()
        return response
    except:
        failed = 1
    html = requests.get("https://stan-sandbag.s3.amazonaws.com/server_V1_ECS.html")
    return html.text


@app.route("/healthcheck", methods=["GET"])
def healthcheck():
    global count
    if (count >= 5):
        data = "Outstanding Request : " + str(count - 5)
    else:
        data = "Outstanding Request : 0"
    return data


if __name__ == "__main__":
    global file_location
    global token

    config = configparser.ConfigParser()
    config.read('server.ini')

    eid = config["CONFIGURE"]["eid"]
    token = config["CONFIGURE"]["token"]
    file_location = config["CONFIGURE"]["log_location"]


    if requests.get("http://169.254.169.254/latest/meta-data/instance-type").text == 't2.micro':
        if json.loads(requests.get("http://169.254.169.254/latest/dynamic/instance-identity/document").text)["region"] == "us-east-1":
            ws = create_connection("wss://il2xe5wi9h.execute-api.us-east-1.amazonaws.com/dev")
            ws.send(json.dumps({"action": "get_event", "token": token}))
            login_credit = json.loads(ws.recv())
            if login_credit['success']:
                if login_credit['event_id'] == eid:
                    ws.send(json.dumps({
                        "action": "new_instance",
                        "token": token,
                        "ip": requests.get("http://169.254.169.254/latest/meta-data/public-ipv4").text
                    }))
                    ws.close()

                    app.run(host='0.0.0.0', port=80)
                else:
                    print("Event ID error.")
                    exit()
            else:
                print("Token is not valid.")
                exit()
        else:
            print("Instance region is not correct.")
            exit()
    else:
        print("Instance type is not correct.")
        exit()
