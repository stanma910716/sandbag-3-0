import requests
import json
import redis
import threading
import time
import hashlib
import boto3
import uuid
from boto3.dynamodb.conditions import Key, Attr
from websocket import create_connection

def send_request(version,url,user_token):
    global r

    Token = str(uuid.uuid4())
    request_id = str(uuid.uuid4())
    if(version == "v1"):
        r.hset(request_id,"token",Token)
        r.hset(request_id,"time",int(time.time()))
    elif(version == "v2"):
        m = hashlib.md5()
        m.update(Token.encode("utf-8"))
        r.hset(request_id,"token",m.hexdigest())
        r.hset(request_id,"time",int(time.time()))
    elif(version == "v3"):
        m = hashlib.sha512()
        m.update(Token.encode("utf-8"))
        r.hset(request_id,"origin",Token)
        r.hset(request_id,"token",m.hexdigest())
        r.hset(request_id,"time",int(time.time()))
    headers = {
        "Version":version,
        "Token":Token,
        "RequestId":request_id
    }
    if(url == ""):
        lost_request(user_token,request_id)
    else:
        try:
            data = requests.get(url,headers=headers,timeout=1)
            if(data.status_code != 200):
                # 大扣分
                lost_request(user_token,request_id)
            else:
                print("Success")
        except:
            # 大扣分
            lost_request(user_token,request_id)
def lost_request(user_token,RID):
    global r
    r.delete(RID)
    ws = create_connection("wss://il2xe5wi9h.execute-api.us-east-1.amazonaws.com/dev")
    ws.send(json.dumps({
        "action":"lost_request",
        "token":user_token
    }))
    print(ws.recv())

if __name__ == "__main__":
    redis_endpoint = json.loads(requests.get("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/redis").text)[0]["data"]
    r = redis.Redis(
        host=redis_endpoint,
        port=6379
    )

    dynamodb = boto3.resource("dynamodb")
    table2 = dynamodb.Table("stan_token")
    print("===  Server Start ===")
    while(True):
        request_power = r.get("request_power").decode("utf-8")
        waf_power = r.get("waf_power").decode("utf-8")
        request_interval = r.get("request_interval").decode("utf-8")
        request_version = r.get("request_version").decode("utf-8")
        users = table2.scan(
            FilterExpression=Attr("role").eq("user")
        )
        if(request_power == "enable"):
            endpoint_data = []
            for user in users["Items"]:
                endpoint = {
                    "endpoint":user["endpoint"],
                    "user_token":user["uuid"]
                }
                endpoint_data.append(endpoint)
            for item in endpoint_data:
                threads = []
                for i in range(5):
                    threads.append(threading.Thread(target = send_request, args = (request_version,item["endpoint"],item["user_token"])))
                    threads[i].start()
        print("Sleep Time : "+str(request_interval))
        time.sleep(float(request_interval))
        # print("===  Server End ===")
        # exit()